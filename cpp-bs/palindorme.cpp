#pragma hdrstop
#pragma argsused

#ifdef _WIN32
#include <tchar.h>
#else
  typedef char _TCHAR;
  #define _tmain main
#endif

#include <stdio.h>
#include <string>
#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;

namespace impl1
{
	bool is_palindrome(const string& text)
	{
		string::const_iterator it1;
		string::const_reverse_iterator it2;
		for(it1 = text.begin(), it2 = text.rbegin(); it1 < it2.base(); ++it1, ++it2)
		{
			if (*it1 != *it2)
				return false;
		}

		return true;
	}
}

namespace impl2
{
	bool is_palindrome(const string& text)
	{
		for(size_t i = 0 ; i < text.size() / 2; ++i)
			if (text[i] != text[text.size() - i -1])
				return false;

		return true;
	}
}

namespace impl3
{
	bool is_palindrome(const string& text)
	{
		auto it_middle = text.begin() + text.size()/2;
		return equal(text.begin(), it_middle, text.rbegin());
	}
}


 int _tmain(int argc, _TCHAR* argv[])
{
	int tab[] = { 1, 2, 3, 4 };
	vector<int> vec = { 4, 3, 2, 1 };

	reverse(vec.begin(), vec.end()); // changes vec

	cout << "ranges are equal: " << equal(vec.begin(), vec.end(), tab) << endl;


	using namespace impl3;

	string text1 = "abba";
	string text2 = "potop";

	// from right to left
	for(auto rit = text2.rbegin(); rit != text2.rend(); ++rit)
		cout << *rit << " ";
	cout << endl;

	// from left to right
	for(auto rit = text2.begin(); rit != text2.end(); ++rit)
		cout << *rit << " ";
	cout << endl;


	string raw_string = R"#(c:\nasz katalog\t)"ext)#";

	cout << "is_palindrome(text1) = " << is_palindrome(text1) << endl;
	cout << "is_palindrome(text2) = " << is_palindrome(text2) << endl;
	cout << R"(is_palindrome("text3") = )" << is_palindrome("text3") << endl;

	system("PAUSE");
}
