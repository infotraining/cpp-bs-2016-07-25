#pragma hdrstop
#pragma argsused

#ifdef _WIN32
#include <tchar.h>
#else
  typedef char _TCHAR;
  #define _tmain main
#endif

#include <stdio.h>
#include <cassert>
#include <iostream>
#include <cmath>
#include <vector>
#include <algorithm>
#include <list>
#include <sstream>
#include <fstream>
#include <exception>

class Vector2D
{
public:
	Vector2D(double x = 0, double y = 0) : x_{x}, y_{y}
	{}

	double x() const
	{
		return x_;
	}

	void set_x(double x)
	{
		x_ = x;
	}

	double y() const
	{
		return y_;
	}

	void set_y(double y)
	{
		y_ = y;
	}

	double length() const
	{
		return std::sqrt(x_ * x_ + y_ * y_);
	}

	Vector2D add(const Vector2D& v) const
	{
		return Vector2D(x_ + v.x(), y_ + v.y());
	}

	Vector2D operator+(const Vector2D& v) const
	{
		return Vector2D(x_ + v.x(), y_ + v.y());
	}

	Vector2D operator-(const Vector2D& v) const
	{
		return Vector2D(x_ - v.x_, y_ - v.y_);
	}

	Vector2D operator-() const
	{
		return Vector2D(-x_, -y_);
	}

	bool operator==(const Vector2D& v) const
	{
		return x_ == v.x_ && y_ == v.y_;
	}

	bool operator!=(const Vector2D& v) const
	{
		return !(*this == v);
	}

	bool operator<(const Vector2D& v) const
	{
		return length() < v.length();
	}

	static const Vector2D& versor_x()
	{
		return versor_x_;
	}

	static const Vector2D& versor_y()
	{
		return versor_y_;
	}
private:
	double x_, y_;
	static const Vector2D versor_x_;
	static const Vector2D versor_y_;
};

ostream& operator<<(ostream& out, const Vector2D& vec)
{
	out << "(" << vec.x() << ", " << vec.y() << ")";

	return out;
}

istream& operator>>(istream& in, Vector2D& vec)
{
	char open_bracket, closing_bracket, separator;
	double x, y;

	in >> open_bracket >> x >> separator >> y >> closing_bracket;

	if (in.fail()
			| (open_bracket != '(')
			| (closing_bracket != ')')
			| (separator != ',') )
	{
		throw std::runtime_error("Bad i/o operation");
	}

	vec.set_x(x);
	vec.set_y(y);

	return in;
}

Vector2D operator*(const Vector2D& v, double value)
{
	return Vector2D(v.x() * value, v.y() * value);
}

Vector2D operator*(double value, const Vector2D& v)
{
	return Vector2D(v.x() * value, v.y() * value);
}

const Vector2D Vector2D::versor_x_ = {1.0, 0.0};
const Vector2D Vector2D::versor_y_ = {0.0, 1.0};

class CompareByLengthDesc
{
public:
	bool operator()(const Vector2D& v1, const Vector2D& v2) const
	{
		return v1.length() > v2.length();
	}
};

using namespace std;

 int _tmain(int argc, _TCHAR* argv[])
{
	string input_text = "(0, 0) (1, 2)";

	stringstream ss(input_text);

	Vector2D v1, v2;
	ss >> v1 >> v2;

	cout << "v1 = " << v1 << endl;
	cout << "v2 = " << v2 << endl;

	cout << "v2 length: " << v2.length() << endl;

	v1.set_x(3);
	v1.set_y(-1);
	auto v3 = v1.add(v2);
	assert(v3.x() == 4);
	assert(v3.y() == 1);

	auto v4 = Vector2D::versor_x();
	assert(v4.x() == 1);
	assert(v4.y() == 0);

	auto v5 = Vector2D::versor_y();
	assert(v5.x() == 0);
	assert(v5.y() == 1);

	vector<Vector2D> vecs = { Vector2D(7.0, 2.5), Vector2D{9.0, 1.4}, {7.5, 9.3} };

	cout << "Lengths of vecs: ";
	for(const auto& vec : vecs)
		cout  << vec.length() << " ";
	cout << endl;

	vecs.push_back(Vector2D(8.0, 10.0));

	vector<Vector2D*> ptr_vecs = { &vecs[0], &vecs[1], &vecs[2] };

	cout << "Vecs: ";
	for(const auto& ptr_vec : ptr_vecs)
		cout << "(" << ptr_vec->x() << ", " << ptr_vec->y() << ")" << " ";
	cout << endl;

// compiler will generate
//	for(auto it = vecs.begin(); it != vecs.end(); ++it)
//	{
//		const Vector2D& vec = *it;
//		cout  << vec.length() << endl;
//	}


	v1 = Vector2D::versor_x();
	v2 = Vector2D::versor_y();

	v5 = v1 + v2;
	assert(v5.x() == 1 && v5.y() == 1);

	auto v6 = v1 - v2;
	assert(v6.x() == 1 && v6.y() == -1);

	auto v7 = -v1;
	assert(v7.x() == -1 && v7.y() == 0);

	auto v8 = v1 * 8;
	assert(v8.x() == 8 && v8.y() == 0);

	auto v9 = 8 * v1;
	assert(v8.x() == 8 && v8.y() == 0);

	auto v10 = Vector2D{1, 5};
	auto v11 = Vector2D{1, 5};
	assert(v10 == v11);
	assert(v10 != v1);

	cout << "v1 = " << v1 << endl;  // (1, 0)

	cout << "\n\n---------------------------------------\n";

	vector<Vector2D> vecs2 = { Vector2D(1, 1), Vector2D(0, 1), Vector2D(7, 10),
							   Vector2D(9, 9), Vector2D(0, 2), Vector2D(20, 0) };

	cout << "vecs2 before sort: ";
	for(const auto& vec : vecs2)
		cout << vec << " ";
	cout << endl;

	sort(vecs2.begin(), vecs2.end());  // default sort:  <

	cout << "vecs2 after sort asc by length: ";
	for(const auto& vec : vecs2)
		cout << vec << " ";
	cout << endl;

	//sort(vecs2.begin(), vecs2.end(), CompareByLengthDesc()); // custom sort
	sort(vecs2.begin(), vecs2.end(),
		 [](const Vector2D& v1, const Vector2D& v2) { return v1.length() > v2.length(); });

	cout << "vecs2 after sort desc by length: ";
	for(const auto& vec : vecs2)
		cout << vec << " ";
	cout << endl;

	std::ofstream fout("data.txt"); // open fstream

	if (!fout)
	{
		cout << "Blad otwarcia pliku..." << endl;
		return -1;
	}

	for(const auto& vec : vecs2)
		fout << vec << endl;

	fout.close();

	system("PAUSE");

	return 0;
}
