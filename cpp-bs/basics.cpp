#pragma hdrstop
#pragma argsused

#include <iostream>
#include <string>
#include <cstdint>
#include <tuple>

using namespace std;

enum class Coffee : uint8_t { espresso = 1, latte, cappuccino };

 int _tmain(int argc, _TCHAR* argv[])
{
	auto i = 42;
	cout << "i: " << i << endl;

	Coffee coffee;
	coffee = Coffee::espresso;
	uint8_t value = static_cast<uint8_t>(coffee);
	cout << "coffee: " << static_cast<uint8_t>(coffee) << endl;

	coffee = static_cast<Coffee>(2);  // coffee = (Coffee)2;
	cout << "coffee: " << static_cast<uint8_t>(coffee) << endl;

	// constants
	const double g = 9.807;
	const double& ref_g = g;

	// string
	string str1 = "text";
	string str2 = "hello";
	string str3 = str1 + ": " + str2 + " world";

	cout << str3 << endl;

	string temp = "text";

	if (str1 == temp)
	{
		cout << "str1 == temp" << endl;
	}

	//string path = R"(c:\nasz folder\text)"; // Raw-string
	//cout << path << endl;

//	string test = R"(line1
//line2
//line3)";

	// references
	int x = 10;
	int& ref_x = x;
	//int* ptr_x = &x;

	ref_x += 10;  // indirect modification
	//(*ptr_x) += 10;

	cout << "x = " << x << endl;

	tuple<int, double, string> t1{8, 2.33, "text"};

	cout << get<0>(t1) << " " << get<1>(t1) << " " << get<2>(t1) << endl;

	system("PAUSE");

	return 0;
}

