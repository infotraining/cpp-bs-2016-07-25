#pragma hdrstop
#pragma argsused

#ifdef _WIN32
#include <tchar.h>
#else
  typedef char _TCHAR;
  #define _tmain main
#endif

#include <stdio.h>
#include <iostream>
#include <algorithm>
#include <string>
#include <vector>
#include <memory>

using namespace std;

//void create_memory_leak()
//{
//	const int N = 1000000;
//
//	int* arr = new int[N];
//
//	fill_n(arr, N, 0);
//}

void no_memory_leak()
{
	const int N = 1000000;

	vector<int> vec(N);

	fill_n(vec.begin(), N, 0);
}

 int _tmain(int argc, _TCHAR* argv[])
{
	// alokacja pamieci
	float* ptr_f1 = new float(10.23); // operator new
	cout << "value: " << (*ptr_f1) << endl;
	*ptr_f1 = 9.44;
	cout << "value: " << (*ptr_f1) << endl;

	// dealokacja pamieci
	delete ptr_f1;
	ptr_f1 = nullptr;

	// dynamiczne tablice
	size_t size = 10;
	string* tab1 = new string[size];  // operator new[]

	// fill with zero
	//	for(size_t i = 0; i < size; ++i)
	//		tab1[i] = "zero";

	// algorithms
	//fill(tab1, tab + size, 0);
	fill_n(tab1, size, "zero");

	tab1[5] = "six";

	for(string* it = tab1; it != tab1 + size; ++it)
		cout << *it << " ";
	cout << endl;

	delete[] tab1; // dealocation of dynamic array

	for(int i = 0; i < 100; ++i)
		no_memory_leak();

	// smart ptrs
	unique_ptr<string> word(new string("Text"));

	cout << *word << endl;
	*word = "String";
	cout << *word << " has size " << word->size() << endl;

	string* observer = word.get();
	cout << *observer << endl;

	// shared_ptr

	shared_ptr<string> outer;
	{
		shared_ptr<string> shared_word = make_shared<string>("Text");

		cout << *shared_word << endl;

		outer = shared_word;
	}

	system("PAUSE");
}
