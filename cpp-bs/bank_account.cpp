#include "bank_account.hpp"

#include <cassert>
#include <iostream>

using namespace std;

namespace Banking
{
	BankAccount::BankAccount(long number, const string& owner, double balance)
		: number_{number}, owner_{owner}, balance_{balance}
	{
	}

	BankAccount::BankAccount(long number, const string& owner)
		:  BankAccount{number, owner, 0.0}  // delegating constructor (C++11)
	{
	}

	long BankAccount::number() const
	{
		return number_;
	}

	std::string BankAccount::owner() const
	{
		return owner_;
	}

	void BankAccount::set_owner(const std::string& owner)
	{
		owner_ = owner;
	}

	double BankAccount::balance() const
	{
		return balance_;
	}

	void BankAccount::deposit(double amount)
	{
		assert(amount >= 0);
		balance_ += amount;
	}

	void BankAccount::withdraw(double amount)
	{
		assert(amount >= 0);

		if (amount > balance_) {
			throw InsufficentFundsException("Insufficent funds error", number_, amount, balance_);
		}

		this->balance_ -= amount;
	}

	void BankAccount::print() const
	{
		cout << "BankAccount(number: " << number_
			 << ", owner: " << owner_
			 << ", balance: " << balance_ << ")" << endl;

	}

	void BankAccount::pay_interest()
	{
		balance_ += balance_ * interest_rate_;
	}

	double BankAccount::interest_rate()
	{
		return interest_rate_;
	}

	void BankAccount::set_interest_rate(double interest_rate)
	{
		interest_rate_ = interest_rate;
	}

	double BankAccount::interest_rate_ = 0.05;


//------------------------------------------------------------------------------

DebitBankAccount::DebitBankAccount(long number, const std::string& owner, double balance, double limit)
	: BankAccount(number, owner, balance), debit_limit_(limit)
{}

DebitBankAccount::DebitBankAccount(long number, const std::string& owner, double limit)
	: BankAccount(number, owner, 0.0), debit_limit_(limit)
{}

void DebitBankAccount::withdraw(double amount)
{
	if (amount > balance() + debit_limit_) {
		throw InsufficentFundsException("Insufficent funds error", number(), amount, balance());;
	}

	balance_ -= amount;
}

double DebitBankAccount::debit_limit() const
{
	return debit_limit_;
}

void DebitBankAccount::set_debit_limit(double limit)
{
	debit_limit_ = limit;
}

}

