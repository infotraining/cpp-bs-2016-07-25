#pragma hdrstop
#pragma argsused

#ifdef _WIN32
#include <tchar.h>
#else
  typedef char _TCHAR;
  #define _tmain main
#endif

#include <stdio.h>
#include <iostream>
#include <sstream>
#include <vector>
#include <memory>

using namespace std;

struct Point
{
	int x, y;

	Point(int x = 0, int y = 0) : x(x), y(y)
	{}
};

ostream& operator<<(ostream& out, const Point& pt)
{
	out << "(" << pt.x << ", " << pt.y << ")";
	return out;
}

class Shape
{
public:
	virtual ~Shape() = default;
	virtual Point coord() const = 0;
	virtual void draw() const = 0;
	virtual void move(int dx, int dy) = 0;
};

class ShapeBase : public Shape
{
protected:
	Point coord_;
public:
	ShapeBase(int x = 0, int y = 0) : coord_(x, y)
	{}

	Point coord() const override
	{
		return coord_;
	}

	void move(int dx, int dy) override
	{
		coord_.x += dx;
		coord_.y += dy;
	}
};

class Circle : public ShapeBase
{
	size_t radius_;
public:
	Circle(int x = 0, int y = 0, size_t r = 0) : ShapeBase(x, y), radius_(r)
	{
	}

	size_t radius() const
	{
		return radius_;
	}

	void set_radius(size_t r)
	{
		radius_ = r;
	}

	void draw() const override
	{
		cout << "Drawing circle at: " << coord_ << " with radius " << radius_ << endl;
	}
};

class Rectangle : public ShapeBase
{
	int w_, h_;
public:
	Rectangle(int x = 0, int y = 0, size_t w = 0, size_t h = 0)
		: ShapeBase(x, y), w_(w), h_(h)
	{}

	size_t width() const
	{
		return w_;
	}

	void set_width(size_t w)
	{
		w_ = w;
	}

	size_t height() const
	{
		return h_;
	}

	void set_height(size_t h)
	{
		h_ = h;
	}

	void draw() const override
	{
		cout << "Drawing a rectangle at: " << coord_
			 << " with w: " << w_
			 << " and h: " << h_ << endl;
	}
};


class Line : public ShapeBase
{
	Point end_;
public:
	Line(int x1 = 0, int y1 = 0, int x2 = 0, int y2 = 0)
		: ShapeBase(x1, y1), end_(x2, y2)
	{}

	Point end() const
	{
		return end_;
	}

	void set_end(const Point& pt)
	{
		end_ = pt;
	}

	void draw() const override
	{
		cout << "Drawing a line from: " << coord_ << " to " << end_ << endl;
	}

	void move(int dx, int dy) override
	{
		ShapeBase::move(dx, dy);  // calling method from a base class

		// extra work
		end_.x += dx;
		end_.y += dy;
	}
};

class Renderer
{
public:
	void render(Shape& shp)
	{
		shp.draw();
	}
};

typedef shared_ptr<Shape> ShapePtr;

class GraphicsDocument
{
	vector<ShapePtr> shapes_;
public:
	void add_shape(ShapePtr shp)
	{
		shapes_.push_back(shp);
	}

	void translate(int dx, int dy)
	{
		for(auto& shp : shapes_)
			shp->move(dx, dy);
	}

	void show() const
	{
		for(const auto& shp : shapes_)
			shp->draw();
	}
};

class GraphicsObject : private Circle
{
public:
	GraphicsObject(int x, int y, size_t r) : Circle(x, y, r)
	{}

	void render()
	{
		draw();
	}
};

 int _tmain(int argc, _TCHAR* argv[])
{
//	Shape shp1(10, 20);  // Shape is an abstract class
//	shp1.draw();
//	shp1.move(5, 10);
//	shp1.draw();

	Circle crc1(50, 10, 100);
	crc1.draw();
	crc1.move(10, 50);
	crc1.set_radius(200);
	crc1.draw();

	Rectangle r1(0, 10, 30, 300);
	r1.draw();
	r1.move(10, 40);
	r1.draw();

	Line l1(10, 50, 21, 235);
	l1.draw();
	l1.move(100, 200);
	l1.draw();

	cout << "\n\n";

	Shape* shp = &l1;
	shp->draw();        // Line::draw()
	shp->move(30, 40);  // Line::move()
	shp->draw();

	Line* ptr_l = dynamic_cast<Line*>(shp);
	if (ptr_l)
	{
		ptr_l->set_end({0, 10});
	}
	shp->draw();

	cout << "\n";

	shp = &crc1;
	shp->draw();          // Circle::draw()
	shp->move(60, 100);   // Shape::move()
	shp->draw();

	Circle* ptr_crc = static_cast<Circle*>(shp); // downcasting
	ptr_crc->set_radius(1000);
	shp->draw();

	cout << "\n";

	Shape& shp_ref = l1;
	shp_ref.draw();         // Rectangle::draw()
	shp_ref.move(89, 100);  // Shape::move()
	shp_ref.draw();

	try
	{
		Rectangle& rect_ref = dynamic_cast<Rectangle&>(shp_ref);
	}
	catch(const bad_cast& e)
	{
		cout << "Caught exception: " << e.what() << endl;
    }

	cout << "\n\n";

	Renderer renderer;
	renderer.render(l1);
	renderer.render(crc1);
	renderer.render(r1);

	cout << "\n\n";

	GraphicsDocument doc;
	doc.add_shape(make_shared<Circle>(1, 10, 50));
	doc.add_shape(make_shared<Rectangle>(55, 33, 10, 50));
	doc.add_shape(make_shared<Line>(89, 324, 12, 11));
	doc.add_shape(make_shared<Circle>(90, 100, 100));

	doc.show();

	doc.translate(10, 20);

	cout << "\nafter translation:\n";

	doc.show();

	system("PAUSE");

	return 0;
}
