#pragma hdrstop
#pragma argsused

#ifdef _WIN32
#include <tchar.h>
#else
  typedef char _TCHAR;
  #define _tmain main
#endif

#include <iostream>
#include <vector>

using namespace std;

bool is_prime(int number)
{
	for(int d = 2; d <= sqrt(static_cast<double>(number)); ++d)
	{
		if (number % d == 0)
			return false;
	}

	return true;
}

// C++11
vector<int> get_primes(int range)
{
	vector<int> primes;

	for(int number = 2; number <= range; ++number)
		if (is_prime(number))
			primes.push_back(number);

	return primes;
}

// C++03
void get_primes(int range, vector<int>& primes)
{
	for(int number = 2; number <= range; ++number)
		if (is_prime(number))
			primes.push_back(number);
}

 int _tmain(int argc, _TCHAR* argv[])
{
	const int N = 200;

	cout << "primes: ";
	for(int number = 2; number <= N; ++number)
		if (is_prime(number))
		{
			cout << number << " ";
		}
	cout << endl;

	cout << "\n---------------------------------\n\n";

	// C++11
	vector<int> primes = get_primes(N);

	// C++03
	primes.clear();
	get_primes(N, primes);

	cout << "primes: ";
	for(vector<int>::const_iterator it = primes.begin(); it != primes.end(); ++it)
		cout << *it << " ";
	cout << endl;

	system("PAUSE");

	return 0;
}
