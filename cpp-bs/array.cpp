#pragma hdrstop
#pragma argsused

#ifdef _WIN32
#include <tchar.h>
#else
  typedef char _TCHAR;
  #define _tmain main
#endif

#include <stdio.h>
#include <iostream>
#include <algorithm>
#include <numeric>
#include <string>
#include <cassert>
#include <list>
#include <vector>
#include <chrono>

using namespace std;

class IndexOutOfRange : public out_of_range
{
	size_t index_;
	size_t size_;
public:
	IndexOutOfRange(const string& msg, size_t index, size_t size)
		: out_of_range(msg), index_(index), size_(size)
	{}

	size_t index() const
	{
		return index_;
	}

	size_t size() const
	{
		return size_;
	}
};

class Array
{
public:
	typedef int* iterator;
	typedef const int* const_iterator;

	Array(size_t size) : size_{size}, array_{new int[size_]}
	{
		fill(array_, array_ + size_, 0);
		cout << "Array(size: " << size_ << ")\n";
	}

	Array(initializer_list<int> il)
		: size_{il.size()}, array_{new int[size_]}
	{
		copy(il.begin(), il.end(), array_);

		cout << "Array(il size: " << size_ << ")\n";
	}

	// copy constructor
	Array(const Array& source)
		: size_{source.size_}, array_{new int[size_]}
	{
		copy(source.array_, source.array_ + size_, array_);

		cout << "Array(cc size: " << size_ << ")\n";
	}

	// copy assignment
	Array& operator=(const Array& source)
	{
		if (this != &source) // avoiding sef-assignment
		{	
			delete[] this->array_;

			array_ = new int[source.size_];
			size_ = source.size_;

			copy(source.array_, source.array_ + size_, array_);
		}
		
		cout << "Array::op=(size: " << size_ << ")\n";

		return *this;
	}

	~Array()
	{
		delete[] array_;
		cout << "~Array(size: " << size_ << ")\n";
	}

	const int& at(size_t index) const
	{
		if (index >= size_)
		{
			throw IndexOutOfRange("Index out of range", index, size_);
		}

		return array_[index];
	}

	int& at(size_t index)
	{
		if (index >= size_)
		{
			throw IndexOutOfRange("Index out of range", index, size_);
		}

		return array_[index];
	}

	const int& operator[](size_t index) const
	{
		return array_[index];
	}

	int& operator[](size_t index)
	{
		return array_[index];
	}

	size_t size() const
	{
		return size_;
	}

	iterator begin()
	{
		return array_;
	}

	const_iterator begin() const
	{
		return array_;
	}

	iterator end()
	{
		return array_ + size_;
	}

	const_iterator end() const
	{
		return array_ + size_;
	}

	bool operator==(const Array& other) const
	{
		return (size_ == other.size_)
				&&  std::equal(begin(), end(), other.begin());
	}

	bool operator!=(const Array& other) const
	{
		return !(*this == other);
	}
private:
	size_t size_;
	int* array_;
};

ostream& operator<<(ostream& out, const Array& array)
{
	out << "[";
	for(const auto& item : array)
		out << item << " ";
	out << "]";

	return out;
}

void print(const Array& arr, const string& prefix)
{
	cout << prefix << ": [ ";

//	for(size_t i = 0; i < arr.size(); ++i)
//		cout << arr[i] << " ";

	for(const auto& item : arr)
		cout << item << " ";

	cout << "]\n";
}


double foo(size_t N)
{
	vector<int> vec(N);

	iota(vec.begin(), vec.end(), 1);

	return accumulate(vec.begin(), vec.end(), 0.0);
}



 int _tmain(int argc, _TCHAR* argv[]) try
{
	const int N = 10000000;

	auto start = chrono::high_resolution_clock::now();

	auto result = foo(N);

	auto end = chrono::high_resolution_clock::now();

	auto interval = chrono::duration_cast<chrono::milliseconds>(end - start).count();

	cout << "Time: " << interval << "ms\n";

	for(int i = 0; i < 10; ++i)
	{
		cout << "\n-------------------------\n\n";

		Array arr1(10);

		for(size_t i = 0; i < arr1.size(); ++i)
			cout << arr1[i] << " ";
		cout << endl;

		arr1[4] = i * 7;
		arr1.at(7) = i * 34;

		for(auto it = arr1.begin(); it != arr1.end(); ++it)
			cout << *it << " ";
		cout << endl;
	}

	cout << "\n\n";

	{
		Array arr1 = { 1, 2, 3, 4, 5, 6 };
		print(arr1, "arr1");

		Array arr2 = arr1; // kopiowanie - cc
		Array arr3(arr1); // kopiowanie - cc

		Array temp = { 8, 46, 77, 43 };
		arr2 = temp; // kopiowanie - op=

		arr2 = arr2;
	}

	{
		Array arr1 = { 1, 2, 3 };
		Array arr2 = { 1, 2, 3 };
		Array arr3 = { 1, 2, 3, 4 };

		assert(arr1 == arr2);
		assert(arr1 != arr3);

		cout << "arr3 = " << arr3 << endl;  // [ 3, 1, 2 ]
	}

	cout << "\n\n\Exceptions:\n";
	{
		Array arr1 = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 };
		vector<int> vec1 = { 6, 34, 235, 645, 346 };

		bool is_ok = false;

		size_t index;

		while(!is_ok)
		{
			try
			{
				cout << "Podaj indeks: ";
				cin >> index;

				auto& item1 =  arr1.at(index);
				cout << "arr1[" << index << "] = " << item1 << endl;

				auto& item2 = vec1.at(index);
				cout << "vec1[" << index << "] = " << item2 << endl;

				is_ok = true;
			}
			catch(const IndexOutOfRange& e)
			{
				cout << "Caught an exception: " << e.what() << endl;
				cout << "Nieprawidlowa wartosc indeksu: " << e.index() << endl;
				cout << "Sprobuj jeszcze raz w przedziale [0, " << e.size() << "]"
					 << endl;
			}
			catch(const out_of_range& e)
			{
				cout << "Caught an exception: " << e.what() << endl;
			}
		}
	}

	system("PAUSE");

	return 0;
}
catch(const exception& e)
{
	cout << "Caught an exception: " << e.what() << endl;
}
