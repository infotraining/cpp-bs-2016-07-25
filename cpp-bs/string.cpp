#pragma hdrstop
#pragma argsused

#ifdef _WIN32
#include <tchar.h>
#else
  typedef char _TCHAR;
  #define _tmain main
#endif

#include <stdio.h>
#include <iostream>
#include <cstdlib>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

class String
{
public:
	String(const char* text)
		: text_{new char[strlen(text)+1]}
	{
		strcpy(text_, text);
	}

	String(const String& source)
		: text_{new char[source.length() + 1]}
	{
		strcpy(text_, source.text_);
	}

	String& operator=(const String& source)
	{
		if (this != &source)
		{
			delete[] text_;

			text_ = new char[source.length() + 1];
			strcpy(text_, source.text_);
		}

		return *this;
	}

	~String()
	{
		delete[] text_;
	}

	size_t length() const
	{
		return strlen(text_);
	}

	const char& at(size_t index) const
	{
		return text_[index];
	}

	char& at(size_t index)
	{
		return text_[index];
	}

	const char& operator[](size_t index) const
	{
		return text_[index];
	}

	char& operator[](size_t index)
	{
		return text_[index];
	}

	bool operator<(const String& s) const
	{
		return strcmp(text_, s.text_) < 0;
	}
private:
	char* text_;
};

// operator <<
ostream& operator<<(ostream& out, const String& s)
{
	for(size_t i = 0; i < s.length(); ++i)
		out << s[i];

	return out;
}

class CompareByLength
{
public:
	bool operator()(const String& s1, const String& s2) const
	{
		return s1.length() < s2.length();
	}
};

 int _tmain(int argc, _TCHAR* argv[])
{
	String str1("text");

	for(int i = 0;  i < str1.length(); ++i)
		cout << str1.at(i);
	cout << endl;

	String str2 = str1;

	for(int i = 0;  i < str2.length(); ++i)
		cout << str2[i];
	cout << endl;

	String temp("temp");
	temp = str1;

	cout << "temp = " << temp << endl;

	vector<String> words = { "one", "two", "three", "four", "five" };

	sort(words.begin(), words.end());  // strcmp

	for(const auto& w : words)
		cout << w << " ";
	cout << endl;

	sort(words.begin(), words.end(), CompareByLength());

	for(const auto& w : words)
		cout << w << " ";
	cout << endl;

	system("PAUSE");

	return 0;
}
