#pragma hdrstop
#pragma argsused

#ifdef _WIN32
#include <tchar.h>
#else
  typedef char _TCHAR;
  #define _tmain main
#endif

#include <iostream>
#include <vector>
#include <string>
#include <cassert>

using namespace std;

void foo_by_value(int x)
{
	++x;
}

void foo_by_ptr(int* ptrx)
{
	if (ptrx) {
		++(*ptrx);
	}
}

void foo_by_reference(int& x)
{
	++x;
	//x++; // the same
}

void print(int x)
{
	cout << "int: " << x << endl;
}

void print(const string& text)
{
	cout << text << endl;
}

void print(const vector<int>& data)
{
	cout << "[ ";
	for(size_t i = 0; i < data.size(); ++i)
		cout << data[i] << " ";
	cout << "]" << endl;
}

void preincrement()
{
	int x = 10;
	int y = ++x;

	assert(x == 11);
	assert(y == 11);
}

void postincrement()
{
	int x = 10;
	int y = x++;

	assert(x == 11);
	assert(y == 10);
}

struct Result
{
	int n;
	int d;
};

Result divide(int x, int y)
{
	Result result;

	result.n = x / y;
	result.d = x % y;

	return result;
}

void divide(int x, int y, int& n, int& d)
{
	n = x / y;
	d = x % y;
}

//function<void()> create_function()
//{
//	return [] { cout << "lambda()" << endl; };
//}

 int _tmain(int argc, _TCHAR* argv[])
{
	preincrement();
	postincrement();

	int xx = 10;
	foo_by_value(xx); // passing by value
	cout << "xx = " << xx << endl;

	foo_by_reference(xx);
	cout << "xx = " << xx << endl;

	vector<int> my_data;
	my_data.push_back(1);
	my_data.push_back(2);
	my_data.push_back(3);
	my_data.push_back(4);

	print(my_data);
	print("text");
	print(3.14);

	int x = 10;
	int y = 3;

	Result result = divide(x, y);
	cout << "n:  " << result.n << "; d:  " << result.d << endl;

	int n, d;
	divide(x, y, n, d);
	cout << "n: " << n << "; d: " << d << endl;

	system("PAUSE");
	return 0;
}
