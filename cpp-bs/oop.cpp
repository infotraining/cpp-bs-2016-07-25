#pragma hdrstop
#pragma argsused

#ifdef _WIN32
#include <tchar.h>
#else
  typedef char _TCHAR;
  #define _tmain main
#endif

#include <stdio.h>
#include <string>
#include <iostream>
#include <cassert>
#include <memory>

#include "bank_account.hpp"

 int _tmain(int argc, _TCHAR* argv[])
{
	using namespace std;

	int x{10}; // C++11

	Banking::BankAccount::set_interest_rate(0.1);
	cout << "Interest_rate: " << Banking::BankAccount::interest_rate() << endl;

	using namespace Banking;

	const BankAccount cba{98534, "Gal Anonim", 3534.0};
	cba.print();
	//cba.set_owner("Zenon Nijaki");

	BankAccount ba1{31243, "Kowalski Jan", 7000.0};
	ba1.print();

	ba1.deposit(2000.0);
	ba1.print();

	ba1.withdraw(400.0);
	ba1.print();

	try
	{
		ba1.withdraw(20000.0);
	}
	catch(const InsufficentFundsException& e)
	{
		cout << "Caught exception: " << e.what() << endl;
		cout << "Bank acocunt number: " << e.number() << endl;
	}

	unique_ptr<BankAccount> ptr_ba(new BankAccount{87236, "Nowak Anna", 2342.0});
	ptr_ba->print();
	ptr_ba->deposit(1000.0);
	ptr_ba->withdraw(1.0);
	ptr_ba->pay_interest();
	ptr_ba->print();

	cout << "\n\n";

	try
	{
		DebitBankAccount debit_account(235235, "Nowakowski Jan", 800, 2000);

		debit_account.print();
		debit_account.deposit(20000);
		debit_account.withdraw(1000);
		debit_account.print();
		debit_account.pay_interest();
		debit_account.withdraw(10000);
	}
	catch(const InsufficentFundsException& e)
	{
		cout << "Caught exception: " << e.what() << endl;
		cout << "Bank acocunt number: " << e.number() << endl;
    }

	std::system("PAUSE");

	return 0;
}
