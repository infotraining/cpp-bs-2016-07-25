#pragma hdrstop
#pragma argsused

#ifdef _WIN32
#include <tchar.h>
#else
  typedef char _TCHAR;
  #define _tmain main
#endif

#include <stdio.h>
#include <iostream>
#include <string>

using namespace std;

template <typename T>
const T& maximum(const T& a, const T& b)
{
	return (a < b) ? b : a;
}


class IndexOutOfRange : public out_of_range
{
	size_t index_;
	size_t size_;
public:
	IndexOutOfRange(const string& msg, size_t index, size_t size)
		: out_of_range(msg), index_(index), size_(size)
	{}

	size_t index() const
	{
		return index_;
	}

	size_t size() const
	{
		return size_;
	}
};

template <typename T>
class Array
{
public:
	typedef T* iterator;
	typedef const T* const_iterator;

	Array(size_t size) : size_{size}, array_{new T[size_]}
	{
		fill(array_, array_ + size_, 0);
		cout << "Array(size: " << size_ << ")\n";
	}

	Array(initializer_list<T> il)
		: size_{il.size()}, array_{new T[size_]}
	{
		copy(il.begin(), il.end(), array_);

		cout << "Array(il size: " << size_ << ")\n";
	}

	// copy constructor
	Array(const Array& source)
		: size_{source.size_}, array_{new T[size_]}
	{
		copy(source.array_, source.array_ + size_, array_);

		cout << "Array(cc size: " << size_ << ")\n";
	}

	// copy assignment
	Array& operator=(const Array& source)
	{
		if (this != &source) // avoiding sef-assignment
		{
			delete[] this->array_;

			array_ = new T[source.size_];
			size_ = source.size_;

			copy(source.array_, source.array_ + size_, array_);
		}

		cout << "Array::op=(size: " << size_ << ")\n";

		return *this;
	}

	~Array()
	{
		delete[] array_;
		cout << "~Array(size: " << size_ << ")\n";
	}

	const T& at(size_t index) const
	{
		if (index >= size_)
		{
			throw IndexOutOfRange("Index out of range", index, size_);
		}

		return array_[index];
	}

	T& at(size_t index)
	{
		if (index >= size_)
		{
			throw IndexOutOfRange("Index out of range", index, size_);
		}

		return array_[index];
	}

	const T& operator[](size_t index) const
	{
		return array_[index];
	}

	T& operator[](size_t index)
	{
		return array_[index];
	}

	size_t size() const
	{
		return size_;
	}

	iterator begin()
	{
		return array_;
	}

	const_iterator begin() const
	{
		return array_;
	}

	iterator end()
	{
		return array_ + size_;
	}

	const_iterator end() const
	{
		return array_ + size_;
	}

	bool operator==(const Array& other) const
	{
		return (size_ == other.size_)
				&&  std::equal(begin(), end(), other.begin());
	}

	bool operator!=(const Array& other) const
	{
		return !(*this == other);
	}
private:
	size_t size_;
	T* array_;
};

template <typename T>
ostream& operator<<(ostream& out, const Array<T>& array)
{
	out << "[ ";
	for(const auto& item : array)
		out << item << " ";
	out << "]";

	return out;
}

 int _tmain(int argc, _TCHAR* argv[])
{
	cout << "max(8, 10) = " << maximum(8, 10) << endl;

	cout << "max(8.9, 10.1) = " << maximum(8.9, 10.1) << endl;

	string s1 = "ola";
	string s2 = "ala";

	cout << "max(s1, s2) = " << maximum(s1, s2) << endl;

	int x = 10;
	double y = 20.2;

	cout << "max(x, y) = " << maximum<double>(x, y) << endl;


	Array<double> numbers = { 6.33, 55.333, 35235.34, 1.3 };

	cout << "numbers: " << numbers << endl;

	Array<string> words = { "one", "two", "three", "four" };

	cout << "words: " << words << endl;

	system("PAUSE");
	return 0;
}
