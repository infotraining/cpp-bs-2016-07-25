#pragma hdrstop
#pragma argsused

#include <iostream>
#include <string>

using namespace std;

typedef void(*ErrorCallback)(const string&);

struct Result
{
	int n;
	int d;
};

Result divide(int x, int y, ErrorCallback error_handler)
{
	Result result;

	if (y == 0)
	{
		error_handler("Division by zero");
		return result;
	}

	result.n = x / y;
	result.d = x % y;

	return result;
}

void show_in_console(const string& msg)
{
	cout << msg << endl;
}

 int _tmain(int argc, _TCHAR* argv[])
{
	// int* ptr2 = nullptr; // C++11

	int x = 10;
	int y = 20;

	int* ptr = 0;
	ptr = &x;
	(*ptr) += 5;

	const int* cptr = &x;
	//(*cptr)++;
	cptr = &y;

	const int* const cptrc = &x;
	//(*cptrc)++;
	//cptrc = &y;

	divide(9, 0, &show_in_console);

	system("PAUSE");
	return 0;
}
