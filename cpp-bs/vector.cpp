#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <array>

using namespace std;

void print(const vector<int>& data)
{
	cout << "[ ";

//	for(size_t i = 0; i < data.size(); ++i)
//		cout << data[i] << " ";

//	for(vector<int>::const_iterator it = data.begin(); it != data.end(); ++it)
//		cout << *it << " ";

	for(auto it = data.begin(); it != data.end(); ++it)
		cout << *it << " ";


	cout << "]" << endl;
}

long calc_sum(int tab[], unsigned int size)
{
	return accumulate(tab, tab + size, 0L);
}

 int main()
{
	// native arrays
	int zeros[10] = {};

	cout << "zeros: ";
	// C++11 range-based-for

	#if defined(_WIN64)
		for(int item : zeros)
			cout << item << " ";
		cout << endl;
	#endif




	int tab[10] = { 7, 4, 1, 2 };
	tab[0] = 9;

	for(/*const*/ int* it = tab; it != tab + 4; ++it)
		cout << *it << " ";
	cout << endl;

	// C++11 range_based_for
	for(int item : tab)
		cout << item << " ";
	cout << endl;

	sort(tab, tab + 4);

	for(/*const*/ int* it = tab; it != tab + 4; ++it)
		cout << *it << " ";
	cout << endl;

	vector<int> my_data = { 3, 44, 2 };

	my_data.push_back(1);
	my_data.push_back(5);
	my_data.push_back(7);

	my_data[0] = -1;
	my_data[1] = 8;

	print(my_data);

    // C++11
	for(const int& item : my_data)
	{
		cout << item << " ";
	}

	sort(my_data.begin(), my_data.end());

	print(my_data);

	// C++11 arrays

	int tab1[10] = { 3, 4, 5 };

	array<int, 10> arr1 = { 3, 4, 5 };

	cout << "arr1.size() = " << arr1.size() << endl;

	for(const auto& item : arr1)
		cout << item << " ";

	auto result = calc_sum(arr1.data(), arr1.size());
	cout << "sum of items in arr1: " << result << endl;

	system("PAUSE");
}
