#pragma hdrstop
#pragma argsused

#ifdef _WIN32
#include <tchar.h>
#else
  typedef char _TCHAR;
  #define _tmain main
#endif

#include <stdio.h>
#include <iostream>
#include <string>
#include <vector>
#include <cstddef>

using namespace std;

template <typename Callable>
auto call(Callable&& c) -> decltype(c())
{
	return c();
}

class Object
{
	vector<int> data_ {1, 2, 3};

public:
	Object() = default;

	Object(initializer_list<int> il) : data_{il.begin(), il.end()}
	{
	}

	~Object()
	{
		cout << "~Object()" << endl;
    }

	Object(const Object& source) : data_{source.data_}
	{
		cout << "Object(cc)" << endl;
	}

	Object& operator=(const Object& source)
	{
		Object temp(source);

		data_.swap(temp.data_);

		cout << "Object op=(cc)" << endl;

		return *this;
	}

	Object(Object&& source) : data_{move(source.data_)}
	{
		cout << "Object(mv)" << endl;
	}

	Object& operator=(Object&& source)
	{
		data_ = move(source.data_);

		cout << "Object op=(mv)" << endl;

		return *this;
	}
};

int _tmain(int argc, _TCHAR* argv[])
{
	auto x = call([] { return 42; });

	auto lambda = [x] { return x * 2; };

	auto result = lambda();

	vector<int> vec = { 1, 2, 3, 4, 5 };

	for(const auto& item : vec)
		cout << item << " ";
	cout << endl;

	vector<Object> objs = { Object(), Object(), Object() };

	objs.push_back(Object());

	system("PAUSE");
}
