#pragma hdrstop
#pragma argsused

#ifdef _WIN32
#include <tchar.h>
#else
  typedef char _TCHAR;
  #define _tmain main
#endif

#include <stdio.h>
#include <iostream>
#include <vector>

using namespace std;

enum class EngineType { diesel, petrol, tdi };

class Engine
{
	EngineType engine_type_;
	long id_;
	bool is_on_ = false;
public:
	Engine(EngineType engine_type, long id)
		: engine_type_{engine_type}, id_{id}
	{
		cout << "Engine(engine_type: " << static_cast<int>(engine_type_)
			 << ", id: " << id_ << ")\n";
	}

	~Engine()
	{
	   cout << "~Engine(engine_type: " << static_cast<int>(engine_type_)
			 << ", id: " << id_ << ")\n";
	}

	void on()
	{
		is_on_ = true;
	}

	void off()
	{
		is_on_ = false;
	}
};

class Car
{
	Engine engine_;
	long id_;
	size_t milage_;
public:
	Car(long id, EngineType engine_type, long engine_id)
		: engine_(engine_type, engine_id), id_(id), milage_(0)
	{
		cout << "Car(id: " << id_ << ")\n";
	}

	~Car()
	{
		cout << "~Car(id: " << id_ << ")\n";
	}

	void drive(size_t distance)
	{
		engine_.on();
		milage_ += distance;
		engine_.off();

		cout << "Driving " <<  distance << "kms" << endl;
	}

	size_t milage() const
	{
		return milage_;
	}
};

 int _tmain(int argc, _TCHAR* argv[])
{
	{
		Car c(1213, EngineType::diesel, 987897);
		c.drive(200);
	}

	system("PAUSE");

	return 0;
}
