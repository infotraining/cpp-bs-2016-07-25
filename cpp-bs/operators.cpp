#pragma hdrstop
#pragma argsused

#ifdef _WIN32
#include <tchar.h>
#else
  typedef char _TCHAR;
  #define _tmain main
#endif

#include <stdio.h>
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

class Integer
{
	int value_ = 0;
public:
	Integer(int v = 0) : value_(v)
	{}

	Integer& operator=(int v)
	{
		value_ = v;

		return *this;
	}

	Integer& operator++()  // ++i
	{
		++value_;

		return *this;
	}

	Integer operator++(int) // i++
	{
		Integer result(*this); // cc

		++value_;
		return result;
	}

//	int value() const
//	{
//		return value_;
//	}

	friend ostream& operator<<(ostream& out, const Integer& i);
};

ostream& operator<<(ostream& out, const Integer& i)
{
	out << "Interger(" << i.value_ << ")";

	return out;
}

class Callable
{
public:
	int operator()(int value) const
	{
		return value;
	}
};

//bool is_greater_than_1000(int x)
//{
//	return x > 1000;
//}

class IsGreaterThan
{
	int value_;
public:
	IsGreaterThan(int value) : value_{value}
	{}

	bool operator()(int x) const
	{
		return x > value_;
	}
};

 int _tmain(int argc, _TCHAR* argv[])
{
	Integer i1 = 8;

	i1 = 10;

	++i1;
	i1++;

	cout << "i1 = " << i1 << endl;

	Callable caller;
	auto result = caller(10);
	cout << "result = " << result << endl;

	IsGreaterThan is_greater_than_10(10);

	cout << "20 > 10 = " << is_greater_than_10(20) << endl;
	cout << "8 > 10 = " << is_greater_than_10(8) << endl;

	vector<int> vec = { 6, 345 ,23, 56, 7, 45, 23243, 12, 5 };

	//IsGreater is_greater_than_1000(1000);
	vector<int>::iterator where_it = find_if(vec.begin(), vec.end(), IsGreaterThan(1000));

	if (where_it != vec.end())
	{
		cout << "I found: " << *where_it << endl;
	}
	else
		cout << "Item not found..." << endl;

	system("PAUSE");

	return 0;
}
