#ifndef BANK_ACCOUNT_HPP
#define BANK_ACCOUNT_HPP

#include <string>
#include <exception>

namespace Banking
{

class InsufficentFundsException : public std::logic_error
{
	long number_;
	double amount_;
	double balance_;
public:
	InsufficentFundsException(const std::string& msg, long number,
							  double amount, double balance)
		: std::logic_error(msg), number_(number), amount_(amount), balance_(balance)
	{}

	long number() const
	{
		return number_;
	}

	double amount() const
	{
		return amount_;
	}

	double balance() const
	{
		return balance_;
	}
};

class BankAccount
{
public:
	BankAccount(long number, const std::string& owner, double balance);
	BankAccount(long number, const std::string& owner);
	virtual ~BankAccount() {}
	long number() const;
	std::string owner() const;
	void set_owner(const std::string& owner);
	double balance() const;
	void pay_interest();
	void deposit(double amount);
	virtual void withdraw(double amount);  // throws InsufficentFundsException
	void print() const;
	static double interest_rate();
	static void set_interest_rate(double interest_rate);
protected:
	double balance_ = 0.0;
private:
	const long number_ = 0;
	std::string owner_ = "unknown";
	static double interest_rate_;
};

class DebitBankAccount : public BankAccount
{
	double debit_limit_;
public:
	DebitBankAccount(long number, const std::string& owner, double balance, double limit);
	DebitBankAccount(long number, const std::string& owner, double limit);
	void withdraw(double amount) override;
	double debit_limit() const;
	void set_debit_limit(double limit);
};

}

#endif
